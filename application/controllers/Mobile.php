<?php
/**
 * Created by PhpStorm.
 * User: Omen
 * Date: 4/4/2021
 * Time: 3:43 PM
 */

class Mobile extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('Db_model','dbm');
        $this->load->model('Mobile_model','mm');
        $this->load->model('Products_model','pm');
    }

    public function register_user(){
        $post = $this->input->post();

        $getUser = $this->dbm->retrieveRowByFKey('users', 'email', $post['email']);
        if(isset($getUser)){
            $res['status'] = '0';
            $res['msg'] = 'User already exist!';
            echo json_encode($res);
        }
        else{
            $user = array(
                'name'=>$post['name'],
                'email'=>$post['email'],
                'password'=>md5($post['password']),
                'confirmation_code' => md5(rand())
                );
            $this->dbm->create('users', $user);
            $link = base_url().'authentication/confirmSignUp/'.$user['confirmation_code'];
            $message = 'Thanks for Sign up. Click on this link to confirm your Email<br>'.$link;
            $this->dbm->sendMail($message, 'Confirmation Email', $user['email']);

            $res['status'] = '1';
            $res['msg'] = 'Registration completed, kindly confirm your email!';
            echo json_encode($res);

        }
    }

    public function login(){
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $user = $this->mm->login($email, $password);
        if(isset($user)){
            if($user->verified == 0){
                $res['status'] = -1;
                $res['msg'] = 'kindly verify your email address.';
//                $res['user'] = $user;
                echo json_encode($res);
            }else{
                $res['status'] = 1;
                $res['msg'] = 'login successfully.';
                $res['user'] = $user;
                echo json_encode($res);
            }
        }else{
            $res['status'] = 0;
            $res['msg'] = 'Email or password are wrong.';
            echo json_encode($res);
        }

    }

    public function get_all_stores(){
        $stores = $this->dbm->retrieveTable('stores', 1);
        $res['base_url'] = base_url('assets/uploaded_images/');
        $res['stores'] = $stores;
        echo json_encode($res);
    }
    public function get_products(){
        $storeId = $this->input->post('store_id');
        $products  = $this->pm->getProducts($storeId);
        $store = $this->dbm->retrieveById('stores', $storeId);
        echo json_encode($products);
    }

    public function get_all_products_brand_wise(){
        $stores = $this->dbm->retrieveTable('stores', 1);
        foreach ($stores as &$st){
            $st->products = $this->pm->getProducts($st->id, 0);
        }
        echo json_encode($stores);
    }
    public function get_all_products_future_fire(){
        $products = $this->pm->getProducts('', 1);
        echo json_encode($products);
    }


}