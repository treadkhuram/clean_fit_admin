<?php

class Users extends CI_Controller
{

    public function __construct(){
        parent::__construct();
        $this->load->model('Db_model','dbm');
    }

    public function index(){
        $data = array(
            'view' => 'users/index',
            'active' => 'users',
            'products'=> $this->dbm->retrieveTable('users', 1)
        );
        $this->load->view('layouts/main_layout', $data);
    }

    public function profile(){
        $storeId = $this->session->userdata('store_id');
        $store = $this->dbm->retrieveById('stores', $storeId);
        $data = array(
            'view' => 'profile/view',
            'active' => 'profile',
            'store' => $store
        );
        $this->load->view('layouts/main_layout', $data);
    }

}