<?php

class Stores extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('Db_model','dbm');
        if(!($this->session->userdata('login'))){
            redirect('login');
        }
    }

    public function index()
    {
        $data = array(
            'view' => 'stores/index',
            'active' => 'stores',
            'stores'=> $this->dbm->retrieveTable('stores', 1)
            );
        $this->load->view('layouts/main_layout', $data);
    }

    public function add(){
        $data = array(
            'view' => 'stores/add',
            'active' => 'stores',
        );

        if(isset($_POST['submit'])){
            $company = array(
                'name'=>$this->input->post('store_name'),
                'phone'=>$this->input->post('phone'),
                'email'=>$this->input->post('email'),
                'website'=>$this->input->post('website'),
                'city'=>$this->input->post('city'),
                'country'=>$this->input->post('country'),
                'password'=> md5($this->input->post('password')),
                'status'=>$this->input->post('status'),
                'address'=>$this->input->post('company_address'),
                'type' => 'store',
            );
            if(isset($_FILES['logo']) && $_FILES['logo']['name'] != ''){
                $company['store_logo'] = $this->dbm->uploadImage('logo');
            }
            $this->dbm->create('stores', $company);
        }

        $this->load->view('layouts/main_layout', $data);
    }
    public function edit($id){

        $old_data = $this->dbm->retrieveById('stores', $id);
        $data = array(
            'view' => 'stores/edit',
            'active' => 'stores',
            'store_data' => $old_data,
        );

        if(isset($_POST['submit'])){

            $company = array(
                'name'=>$this->input->post('store_name'),
                'phone'=>$this->input->post('phone'),
                'email'=>$this->input->post('email'),
                'website'=>$this->input->post('website'),
                'city'=>$this->input->post('city'),
                'country'=>$this->input->post('country'),
                'password'=> md5($this->input->post('password')),
                'status'=>$this->input->post('status'),
                'address'=>$this->input->post('company_address'),
                'type' => 'store',
            );

            if(isset($_FILES['logo']) && $_FILES['logo']['name'] != ''){
                $company['store_logo'] = $this->dbm->uploadImage('logo');
            }
            $this->dbm->update('stores', $id, $company);
            redirect('Stores/edit/'.$id);
        }
        $this->load->view('layouts/main_layout', $data);
    }

    public function deleteStore($id){
        $this->dbm->softDelete('stores', $id);
        redirect('stores');
    }

}