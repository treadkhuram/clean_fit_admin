<?php

class Orders extends CI_Controller
{

    public function __construct(){
        parent::__construct();
        $this->load->model('Db_model','dbm');
    }

    public function index(){
        $data = array(
            'view' => 'orders/index',
            'active' => 'orders',
            'products'=> $this->dbm->retrieveTable('products', 1)
        );
        $this->load->view('layouts/main_layout', $data);
    }

}