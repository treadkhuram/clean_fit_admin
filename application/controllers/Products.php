<?php

class Products extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Db_model','dbm');
        $this->load->model('Products_model','pm');
        if(!($this->session->userdata('login'))){
            redirect('login');
        }
    }

    public function index(){
        $data = array(
            'view' => 'products/index',
            'active' => 'products',
            'products'=> $this->dbm->retrieveTable('products', 1)
        );
        $this->load->view('layouts/main_layout', $data);
    }

    public function add(){

        $data = array(
            'view' => 'products/add',
            'active' => 'products',
            'colors'=>$this->dbm->retrieveTable('colors'),
            'sizes'=>$this->dbm->retrieveTable('sizes'),
            'stores'=> $this->dbm->retrieveTable('stores', 1)
        );
        
        $this->load->view('layouts/main_layout', $data);
    }
    
    public function save()
    {
        $post = $this->input->post();   
        $colors = array(
            'black', 'white', 'red', 'yellow', 'blue', 'orange', 'purple'
        );
        $color_data = array();
        foreach($colors as $k=>$color){
            $color_data[$color] = $post[$color];
        }
        $color_data = json_encode($color_data);
        $dump = array();
        $images = $_FILES['images'];
        if(isset($images)){
            $row = $this->dbm->uploadMultiple('images');
            $dump = json_encode($row);                    
        }        
        $data = array(
            'product_no' => $post['product_no'],
            'name' => $post['name'],
            'product_type' => $post['product_type'],
            'price' => $post['price'],
            'product_info' => $post['product_info'],
            'product_composition'=> $post['product_composition'],
            'product_shipping'=> $post['product_shipping'],
            'total_qty'=> $post['total_qty'],
            'fit_and_size'=>$post['fit_and_size'],
            'colors'=> $color_data,
            'images'=> $dump,
            'active'=> '1',
            'store_id' => $this->session->userdata('store_id'),
        );
        
        $this->pm->addProducts($data);
        redirect('Products/add');
    }

    public function edit($id){
        $product_data = $this->dbm->retrieveById('products', $id);
        $data = array(
            'view' => 'products/edit',
            'active' => 'products',
            'products' => $product_data,
        );

        $this->load->view('layouts/main_layout', $data);
    }
    public function update()
    {
        $id = $this->input->post('id');
        $product_data = $this->dbm->retrieveById('products', $id);
        $post = $this->input->post();
        $colors = array(
            'black', 'white', 'red', 'yellow', 'blue', 'orange', 'purple'
        );
        $color_data = array();
        foreach($colors as $k=>$color){
            $color_data[$color] = $post[$color];
        }
        $color_data = json_encode($color_data);
        $data = array(
            'product_no' => $post['product_no'],
            'name' => $post['name'],
            'product_type' => $post['product_type'],
            'price' => $post['price'],
            'product_info' => $post['product_info'],
            'product_composition'=> $post['product_composition'],
            'product_shipping'=> $post['product_shipping'],
            'total_qty'=> $post['total_qty'],
            'fit_and_size'=>$post['fit_and_size'],
            'colors'=> $color_data,
            'active'=> '1',
        );
        
        if($_FILES['images']['name'][0] != ''){
            $images = $_FILES['images'];
            if(isset($images)){
                $row = $this->dbm->uploadMultiple('images');
                $data['images'] = json_encode($row);                    
            }
        }else{
            // print_r($product_data['images']);
            $data['images'] = $product_data->images;
        }
        
        $this->dbm->update('products', $id, $data);
        redirect('Products/index');       
    }

    public function deleteProduct($id){
        $this->dbm->softDelete('products', $id);
        redirect('products');
    }

    public function removeImg()
    {
       $img_name = $this->input->post('img');
       $id = $this->input->post('id');
       $product_data = $this->dbm->retrieveById('products', $id);
        $data = array();
        $images_data = json_decode($product_data->images);
        foreach($images_data as $img){
            if($img != $img_name){
                array_push($data, $img); 
            }
        } 
        $data = json_encode($data);
        $this->pm->updateImg('products', $product_data->id, $data);
        redirect('Products/edit/'.$product_data->id);
    }







}