<?php


class Login extends CI_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('Login_model', 'lm');
        
    }
  
    public function index() 
    {
        $this->load->view('admin/login');
    }
    
    public function authentication() 
    {
        
        $this->form_validation->set_rules('username','Username','required|trim');
        $this->form_validation->set_rules('password','Password','required|trim');
      
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $check1 = $this->lm->checkstore($username, ($password));
        
        if($check1 == true)
        {
            if($check1->type == 'store'){
                $data = array(
                    'username' => $username,
                    'store_logged_in' => TRUE,
                    'store_id' => $check1->id,
                    'store_name' => $check1->name,
                    'login' => true,
                );
            }else{
                $data = array(
                    'username' => $username,
                    'admin_logged_in' => true,
                    'store_id' => $check1->id,
                    'admin_name' => $check1->name,
                    'login' => true,
                );
            }
            $this->session->set_userdata($data);
            $this->session->set_userdata('store_data',$check1);
            redirect('admin');

        }else{  
                $massage = array(
                'attenticaion' => '<p class="alert alert-danger">Incorrect Username or Password</p>'
            );
            $this->session->set_flashdata($massage);
            redirect('login');
        }
    }
    
    public function logout()
    {
        $this->session->unset_userdata('store_logged_in');
        $this->session->unset_userdata('admin_logged_in');
        $this->session->unset_userdata('login');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('store_data');
        redirect('login');
    }
}
