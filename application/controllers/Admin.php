<?php

class Admin extends CI_Controller{

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('Login_model', 'lm');
        $this->load->model('Db_model', 'dbm');
        if($this->session->userdata('login') != true){
            redirect('Login');
        }
        
    }

    public function index()
    {
        $data = array(
            'view' => 'admin/dashboard',
            'active' => 'dashboard',
        );

        if($this->session->userdata('admin_logged_in')){
            
            $data['total_stores'] = count($this->dbm->retrieveTable('stores'));
            $data['total_users'] = count($this->dbm->retrieveTable('users'));

        }else{
            $data['total_products'] = count($this->dbm->retrieveTable('products'));
            // $data['total_orders'] = count($this->dbm->retrieveTable('products'));
        }

        $this->load->view('layouts/main_layout', $data);
    }


}




?>