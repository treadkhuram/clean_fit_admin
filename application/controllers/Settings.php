<?php

class Settings extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model('Db_model','dbm');
    }

    public function color(){
        $data = array(
            'view' => 'settings/color',
            'active' => 'colors',
            'color'=> $this->dbm->retrieveTable('colors')
        );
        $this->load->view('layouts/main_layout', $data);
    }

    public function add_color(){
        $data = array(
            'view' => 'settings/add_color',
            'active' => 'colors',
        );

        if (isset($_POST['submit'])){
            $store=array(
                'name'=>$this->input->post('color_name'),
                'code'=>$this->input->post('color_name'),
            );
            $this->dbm->create('colors',$store);
        }

        $this->load->view('layouts/main_layout', $data);
    }

    public function size(){
        $data = array(
            'view' => 'settings/size',
            'active' => 'sizes',
            'size'=> $this->dbm->retrieveTable('sizes')
        );
        $this->load->view('layouts/main_layout', $data);
    }

    public function add_size(){
        $data = array(
            'view' => 'settings/add_size',
            'active' => 'size',
        );

        $size=array(
            'name'=>$this->input->post('name'),
            'chest'=>$this->input->post('chest'),
            'waist'=>$this->input->post('waist'),
            'lenght'=>$this->input->post('lenght'),
        );
        $this->dbm->create('sizes',$size);

        $this->load->view('layouts/main_layout', $data);
    }

    public function deleteColor($id){
        $this->dbm->delete('colors', $id);
        redirect('settings/color');
    }

    public function deleteSize($id){
        $this->dbm->delete('sizes', $id);
        redirect('settings/size');
    }



}