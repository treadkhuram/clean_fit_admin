<?php

class Db_model extends CI_Model{

    public function retrieveTable($table, $active = 0){
      $this->db->select('*');
      $this->db->from($table);
      if($active == 1){
        $this->db->where('active', 1);
      }
      return $this->db->get()->result();
    }

       public function retrieveById($table, $id){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function retrieveTableByFKey($table, $columnName, $val){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($columnName, $val);
        return $this->db->get()->result();
    }
  public function retrieveRowByFKey($table, $columnName, $val){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($columnName, $val);
        return $this->db->get()->row();
    }

    public function create($tableName, $data){
        return $this->db->insert($tableName, $data);
    }

    public function createGetId($tableName, $data){
        $this->db->insert($tableName, $data);
        return $this->db->insert_id();
    }

  public function delete($table, $id){
    $this->db->where("id",$id);
    $this->db->delete($table); 
  }

    public function softDelete($table, $id){
        $this->db->set("active",0);
        $this->db->where("id",$id);
        $this->db->update($table);
    }

    public function deleteByFk($table, $column, $id){
        $this->db->where($column,$id);
        $this->db->delete($table);
    }
  
  public function remove($table, $data){
    $this->db->where($data);
    $this->db->delete($table); 
  }
  
  public function update($table, $id, $data){
    $this->db->where('id', $id);
    return $this->db->update($table, $data);
  }

    public function updateByFk($table, $column, $id, $data){
        $this->db->where($column, $id);
        return $this->db->update($table, $data);
    }

    public function checkValueExist($table, $data){
        $q = $this->db->where($data)->count_all_results($table);
        return $q;
    }
  
  public function tableRowsCount($table){
        $q = $this->db->count_all_results($table);
        return $q;
    }
  
  public function tableRowsCountByKey($table,$columnName, $val){
        $this->db->select('*')->from($table)->where($columnName, $val);
        $q = $this->db->count_all_results();
        return $q;
    }

    function sendMail($message, $subject, $to, $from='mailto:admin@sentaxlab.com'){
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
        $headers .= "From: clean-Fit"."<".$from.">\r\n" .
            "Reply-To: ".$from . "\r\n" .
            "X-Mailer: PHP/" . phpversion();

        $emailSent = mail($to,$subject,$message,$headers);
        return $emailSent;
    }


    public function uploadImage($photo_name){
        $return_photo = 'defualt.png';
        $config['upload_path'] = './assets/uploaded_images/';
        $config['allowed_types'] = 'jpg|png';
        $config['overwrite'] = FALSE;
        $config['encrypt_name'] = TRUE;
        $this->upload->initialize($config);
        if ($this->upload->do_upload($photo_name)) {
            $return_photo = $this->upload->data('file_name');
        }
        return $return_photo;
    }

    public function uploadMultiple($field_name){
    
        $files = $_FILES;
        $cpt = count($_FILES[$field_name]['name']);//count for number of image files
        $config['upload_path'] = './assets/uploaded_images/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['overwrite'] = FALSE;
        $config['encrypt_name'] = TRUE;
        $image_name =array();
        for($i=0; $i<$cpt; $i++)
        {
            $_FILES[$field_name]['name']= $files[$field_name]['name'][$i];
            $_FILES[$field_name]['type']= $files[$field_name]['type'][$i];
            $_FILES[$field_name]['tmp_name'] = $files[$field_name]['tmp_name'][$i];
            $_FILES[$field_name]['error']= $files[$field_name]['error'][$i];
            $_FILES[$field_name]['size'] = $files[$field_name]['size'][$i];

            $this->upload->initialize($config);
            $this->upload->do_upload($field_name);

            $data = array('upload_data' => $this->upload->data());
            $image_name[]=$data['upload_data']['file_name'];

        }
        return $image_name;//all images name which is uploaded
    }


}

?>