<?php

class Products_model extends Db_model
{
    public function addProduct($post){
        $product['name'] = $post['product_name'];
        $product['price'] = $post['price'];
        $product['quantity'] = $post['quantity'];
        $product['sku'] = $post['sku'];
        $product['store_id'] = $post['store'];
        $product['product_info'] = $post['information'];
        $product['product_composition'] = $post['composition'];
        $product['product_shipping'] = $post['shipping'];
        $product['images'] = '[]';

        $images = $_FILES['images'];
        if(isset($images)){
            $r = $this->uploadMultiple('images');
            $product['images'] = json_encode($r);
        }

        $productId = $this->createGetId('products', $product);


        $colors = $post['color'];
        foreach ($colors as $col){
            $colorData['product_id'] = $productId;
            $colorData['color_id'] = $col;
            $this->create('products_colors',$colorData);
        }
        $sizes = $post['size'];
        foreach ($sizes as $size){
            $sizeData['product_id'] = $productId;
            $sizeData['size_id'] = $size;
            $this->create('products_sizes', $sizeData);
        }
    }
   
    public function addProducts($data)
    {
       return $this->db->insert('products', $data);
    }




    public function getProducts($storeId='', $productType=0){
        $this->db->select('p.*, s.id as store_id, s.name as store_name, s.store_logo')
            ->from('products p')
            ->join('stores s', 's.id = p.store_id');
        if($storeId != ''){
            $this->db->where('store_id', $storeId);
        }
        $this->db->where('product_type',$productType);
        $products = $this->db->get()->result();
//        foreach ($products as &$pro){
//            $pro->colors = $this->getProductColor($pro->id);
//            $pro->sizes = $this->getProductSize($pro->id);
//        }
        return $products;
    }

    public function getProductColor($productId){
        $this->db->select('c.*')
            ->from('products_colors p')
            ->join('colors c', 'p.color_id = c.id' )
            ->where('p.product_id', $productId);
        return $this->db->get()->result();
    }

    public function getProductSize($productId){
        $this->db->select('s.*')
            ->from('products_sizes p')
            ->join('sizes s', 'p.size_id = s.id' )
            ->where('p.product_id', $productId);
        return $this->db->get()->result();
    }

    public function updateImg($table, $id, $data){
        
        $this->db->set('images', $data);
        $this->db->where('id', $id);
        $q = $this->db->update($table);

        if($q){
            return 1;
        }else{
            return 0;
        }
    }

}