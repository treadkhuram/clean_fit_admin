<div class="main-panel">
    <div class="main-content">
        <div class="content-wrapper">
            <div class="container-fluid">
                <section id="horizontal-form-layouts">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title-wrap bar-warning">
                                        <h4 class="card-title" id="horz-layout-colored-controls">Add Size</h4>
                                    </div>
                                    <p class="mb-0">You can add new size here by providing the below listed
                                        details.</p>
                                </div>
                                <div class="card-body">
                                    <div class="px-3">
                                        <form action="<?= base_url(); ?>settings/add_size" method="POST"
                                              class="form form-horizontal" enctype="multipart/form-data">
                                            <div class="form-body">
                                                <h4 class="form-section">
                                                    <i class="icon-direction"></i>Size</h4>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"> Name: </label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control border-primary"
                                                                       name="name" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control"> Chest: </label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control border-primary"
                                                                       name="chest" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Waist : </label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control border-primary"
                                                                       name="waist" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Length : </label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control border-primary"
                                                                       name="lenght" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                              
                                            <div class="form-actions right">
                                                <button type="submit" name="submit" class="btn btn-success">Submit</button>
                                            </div>
                                    </div>

                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
            </div>

            </section>
        </div>
    </div>
</div>
