
<div class="main-panel">
    <div class="main-content">
        <div class="content-wrapper">
            <div class="container-fluid"><!--Extended Table starts-->
                <div class="row">
                    <div class="col-8">
                        <h2 class="content-header">Size</h2>
                        <p class="content-sub-header">You can Add, Edit, Delete the Size.</p>
                    </div>
                    <div class="col-4">
                        <h2 class="content-header pull-right">
                            <a href="<?= base_url('settings/add_size')?>" class="btn btn-social btn-round  btn-outline-facebook">
                                <span class="ft-plus"></span> Add Size</a>
                        </h2>
                    </div>
                </div>

                <section id="shopping-cart">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-block">
                                        <div class="table-responsive">
                                            <table id="recent-orders" class="table table-hover table-xl mb-0">
                                                <thead>
                                                <tr>
                                                    <th class="border-top-0">#</th>
                                                    <th class="border-top-0"> Name</th>
                                                    <th class="border-top-0"> Chest</th>
                                                    <th class="border-top-0"> Waist</th>
                                                    <th class="border-top-0">Length</th>
                                                    <th class="border-top-0">Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($size as $key=>$pro){ ?>
                                                    <tr>
                                                        <td><?= ($key+1) ?></td>
                                                        <td><?= $pro->name ?></td>
                                                        <td><?= $pro->chest ?></td>
                                                        <td><?= $pro->waist ?></td>
                                                        <td><?= $pro->lenght ?></td>
                                                        <td>
                                                            <a href="" class="success p-0" data-original-title="" title="">
                                                                <i class="fa fa-pencil font-medium-3 mr-2"></i>
                                                            </a>
                                                            <a onclick="confirmDelete('<?= base_url().'settings/deleteSize/'.$pro->id ?>')"class="danger p-0">
                                                                <i class="fa fa-trash-o font-medium-3 mr-2"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
