<div class="main-panel">
    <div class="main-content">
        <div class="content-wrapper">
            <div class="container-fluid"><!-- User Profile Starts-->


                <section id="user-area">
                    <div class="row">

                        <div class="col-xl-12 col-lg-12">
                            <!--About div starts-->


                            <div id="about">
                                <div class="row">

                                    <div class="col-sm-12">
                                        <div class="card">

                                            <div class="card-body">
                                                <div class="card-block">
                                                    <div class="row">
                                                        <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                            <div class="align-self-center halfway-fab text-center">
                                                                <a class="profile-image">
                                                                    <img src="<?= base_url().'assets/uploaded_images/'.$store->store_logo ?>" class="rounded-circle img-border gradient-summer width-100" alt="Card image">
                                                                </a>
                                                            </div>
                                                            <div class="text-center">
                                                                <span class="font-medium-2 text-uppercase"><?= $store->name ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-5 col-md-5 col-sm-12 col-12">
                                                            <ul class="no-list-style">
                                                                <li class="mb-2">
                                                                    <span class="primary text-bold-500"><a>Vendor Name</a></span>

                                                                    <span class="line-height-2 display-block overflow-hidden"><?= $store->name ?></span>
                                                                </li>
                                                                <li class="mb-2">
                                                                    <span class="primary text-bold-500"><a>Email </a></span>

                                                                    <span class="line-height-2 display-block overflow-hidden"><?= $store->email ?></span>
                                                                </li>
                                                                <li class="mb-2">
                                                                    <span class="primary text-bold-500"><a>Country </a></span>

                                                                    <span class="line-height-2 display-block overflow-hidden"><?= $store->country ?></span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-lg-5 col-md-5 col-sm-12 col-12">
                                                            <ul class="no-list-style">
                                                                <li class="mb-2">
                                                                    <span class="primary text-bold-500"><a>Contact No</a></span>

                                                                    <span class="line-height-2 display-block overflow-hidden"><?= $store->phone ?></span>
                                                                </li>
                                                                <li class="mb-2">
                                                                    <span class="primary text-bold-500"><a> City</a></span>
                                                                    <span class="line-height-2 display-block overflow-hidden"><?= $store->city ?></span>
                                                                </li>


                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!--About div ends-->


                        </div>
                    </div>
                </section>
                <!--User Profile Starts-->
            </div>
        </div>
    </div>
</div>