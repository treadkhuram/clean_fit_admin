
<!DOCTYPE html>
<html lang="en" class="loading">
  
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
    <title>Dashboard</title>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    

    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900%7CMontserrat:300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>fonts/simple-line-icons/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>vendors/css/perfect-scrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>vendors/css/prism.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>vendors/css/chartist.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>vendors/css/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>widgEditor/widgEditor.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>widgEditor/widgEditor.js">

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>css/app.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>vendors/css/wizard.css">

    <!-- <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900%7CMontserrat:300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>fonts/simple-line-icons/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>vendors/css/perfect-scrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>vendors/css/prism.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>css/app.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>assets/vendors/css/wizard.css"> -->
 
  </head>
  <body data-col="2-columns" class=" 2-columns ">
    <div class="wrapper">


      <div data-active-color="black" data-background-color="white" data-image="" class="app-sidebar">
        <div class="sidebar-header">
          <div class="logo clearfix p-1">
              <a href="#" class="logo-text float-left">
                  <span class="text align-middle">Clean Fitted</span>
              </a>
            </div>
        </div>
        <div class="sidebar-content">
          <div class="nav-container">
            <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">

                <li class=" <?= $active == 'dashboard' ? 'active':'' ?> nav-item">
                    <a href="<?= base_url('admin')?>">
                        <i class="icon-home"></i><span data-i18n="" class="menu-title">Dashboard</span>
                    </a>
                </li>
                <?php if($this->session->userdata('admin_logged_in')){ ?>
                <li class="<?= $active == 'stores' ? 'active':'' ?> nav-item">
                  <a href="<?= base_url('stores')?>">
                      <i class="icon-grid"></i><span data-i18n="" class="menu-title">Stores</span>
                  </a>
                </li>
                <li class="<?= $active == 'users' ? 'active':'' ?> nav-item">
                  <a href="<?= base_url('users')?>">
                    <i class="icon-user"></i><span data-i18n="" class="menu-title">Users</span>
                  </a>
                </li>
                <?php }?>
                <?php if($this->session->userdata('store_logged_in')){ ?>
                <li class="<?= $active == 'products' ? 'active':'' ?> nav-item">
                  <a href="<?= base_url('products')?>">
                      <i class="icon-folder"></i><span data-i18n="" class="menu-title">Products</span>
                  </a>
                </li>
                <li class="<?= $active == 'orders' ? 'active':'' ?> nav-item">
                    <a href="<?= base_url('orders')?>">
                        <i class="icon-basket"></i><span data-i18n="" class="menu-title">Orders</span>
                    </a>
                </li>
                <?php }?>
                <!-- <li class="<?= $active == 'colors' ? 'active':'' ?> nav-item">
                  <a href="<?= base_url('settings/color')?>">
                      <i class="icon-folder"></i><span data-i18n="" class="menu-title">Colors</span>
                  </a>
                </li>
                <li class="<?= $active == 'sizes' ? 'active':'' ?> nav-item">
                  <a href="<?= base_url('settings/size')?>">
                      <i class="icon-folder"></i><span data-i18n="" class="menu-title">Size</span>
                  </a>
                </li> -->
             
            </ul>
          </div>
        </div>
        <div class="sidebar-background"></div>
      </div>

      <nav class="navbar navbar-expand-lg navbar-light bg-faded">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" data-toggle="collapse" class="navbar-toggle d-lg-none float-left"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><span class="d-lg-none navbar-right navbar-collapse-toggle"><a class="open-navbar-container"><i class="ft-more-vertical"></i></a></span><a id="navbar-fullscreen" href="javascript:;" class="mr-2 display-inline-block apptogglefullscreen"><i class="ft-maximize blue-grey darken-4 toggleClass"></i>
              <p class="d-none">fullscreen</p></a>

          </div>
          
          <div class="navbar-container">
            <div id="navbarSupportedContent" class="collapse navbar-collapse">
              <ul class="navbar-nav">
                

                <li class="dropdown nav-item mr-0"><a id="dropdownBasic3" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-user-link dropdown-toggle">
                        <img id="navbar-avatar" height="20" src="<?= base_url().'assets/uploaded_images/'. $this->session->userdata('store_data')->store_logo ?>" alt="avatar"/>
                    </a>
                  <div aria-labelledby="dropdownBasic3" class="dropdown-menu dropdown-menu-right">
                    <div class="arrow_box_right"><a href="<?= base_url('users/profile')?>" class="dropdown-item py-1"><i class="ft-edit mr-2"></i><span>My Profile</span></a>

                      <div class="dropdown-divider"></div><a href="<?= base_url();?>Login/logout" class="dropdown-item"><i class="ft-power mr-2"></i><span>Logout</span></a>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>



      <?php $this->load->view($view); ?>


        <footer class="footer footer-static footer-light">
          <p class="clearfix text-muted text-center px-2"><span>Copyright  &copy; 2021 <a href="#" id="pixinventLink"  class="text-bold-800 primary darken-2">I Find </a>, All rights reserved. </span></p>
        </footer>

      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->



     <!-- BEGIN VENDOR JS-->
    <script src="<?php echo base_url('assets/'); ?>vendors/js/core/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>vendors/js/core/popper.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>vendors/js/core/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>vendors/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>vendors/js/prism.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>vendors/js/jquery.matchHeight-min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>vendors/js/screenfull.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>vendors/js/pace/pace.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="<?php echo base_url('assets/'); ?>vendors/js/chartist.min.js"></script>
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="<?php echo base_url('assets/'); ?>vendors/js/jquery.steps.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>vendors/js/jquery.validate.min.js"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN CONVEX JS-->
    <script src="<?php echo base_url('assets/'); ?>js/app-sidebar.js"></script>
    <script src="<?php echo base_url('assets/'); ?>js/notification-sidebar.js"></script>
    <script src="<?php echo base_url('assets/'); ?>js/customizer.js"></script>
    <!-- END CONVEX JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="<?php echo base_url('assets/'); ?>js/dashboard-analytics.js"></script>
    <!-- END PAGE LEVEL JS-->
    <script src="<?php echo base_url('assets/'); ?>js/dashboard-ecommerce.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>select/form-select2.js"></script>
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="<?php echo base_url('assets/'); ?>js/wizard-steps.js"></script>
    <!-- END PAGE LEVEL JS-->



    <!-- END PAGE LEVEL JS-->
    <!-- <script src="<?php echo base_url('assets/'); ?>vendors/js/core/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>vendors/js/core/popper.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>vendors/js/core/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>vendors/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>vendors/js/prism.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>vendors/js/jquery.matchHeight-min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>vendors/js/screenfull.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>vendors/js/pace/pace.min.js"></script> -->
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- <script src="<?php echo base_url('assets/'); ?>vendors/js/chartist.min.js"></script> -->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN CONVEX JS-->
    <!-- <script src="<?php echo base_url('assets/'); ?>js/app-sidebar.js"></script>
    <script src="<?php echo base_url('assets/'); ?>js/notification-sidebar.js"></script>
    <script src="<?php echo base_url('assets/'); ?>js/customizer.js"></script> -->
    <!-- END CONVEX JS-->
    <!-- BEGIN PAGE LEVEL JS-->

    <script>
        function confirmDelete(url){
            var r = confirm("Are you sure, You wants to delete ?");
            if (r == true) {
                window.open(url,"_self");
            }
        }
    </script>

  </body>

</html>