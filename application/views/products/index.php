
<div class="main-panel">
    <div class="main-content">
        <div class="content-wrapper">
            <div class="container-fluid"><!--Extended Table starts-->
                <div class="row">
                    <div class="col-8">
                        <h2 class="content-header">Products</h2>
                        <p class="content-sub-header">You can Add, Edit, Delete the Products.</p>
                    </div>
                    <div class="col-4">
                        <h2 class="content-header pull-right">
                            <a href="<?= base_url('products/add')?>" class="btn btn-social btn-round  btn-outline-facebook">
                                <span class="ft-plus"></span> Add Product</a>
                        </h2>
                    </div>
                </div>

                <section id="shopping-cart">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-block">
                                        <div class="table-responsive">
                                            <table id="recent-orders" class="table table-hover table-xl mb-0">
                                                <thead>
                                                <tr>
                                                    <th class="border-top-0">#</th>
                                                    <th class="border-top-0">Name</th>
                                                    <th class="border-top-0">Pictures</th>
                                                    <th class="border-top-0">Quantity</th>
                                                    <th class="border-top-0">Categories</th>
                                                    <th class="border-top-0">Price</th>
                                                    <th class="border-top-0">Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($products as $key=>$pro){ ?>
                                                    <tr>
                                                        <td><?= ($key+1) ?></td>
                                                        <td><?= $pro->name ?></td>
                                                        <td>
                                                            <ul class="list-unstyled users-list m-0">
                                                                <?php
                                                                $images = json_decode($pro->images);
                                                                foreach ($images as $img){
                                                                    if($img != ''){
                                                                    ?>
                                                                    <li data-toggle="tooltip" data-popup="tooltip-custom" data-original-title="Kimberly Simmons" class="avatar avatar-sm pull-up">
                                                                        <img src="<?= base_url().'assets/uploaded_images/'.$img ?>" class="media-object rounded-circle no-border-top-radius no-border-bottom-radius">
                                                                    </li>
                                                                <?php } } ?>
                                                            </ul>
                                                        </td>
                                                        <td><?= $pro->quantity ?></td>
                                                        <td><?= $pro->product_type == 0 ? '<button type="button" class="btn btn-sm btn-outline-danger round">Current Stock</button>':'<button type="button" class="btn btn-sm btn-outline-danger round">Future Fire</button>' ?></td>
                                                        <td><?= $pro->price ?></td>
                                                        <td>
                                                            <a href="<?= base_url()?>products/edit/<?= $pro->id?>" class="success p-0" data-original-title="" title="">
                                                                <i class="fa fa-pencil font-medium-3 mr-2"></i>
                                                            </a>
                                                            <a onclick="confirmDelete('<?= base_url().'products/deleteProduct/'.$pro->id ?>')"class="danger p-0">
                                                                <i class="fa fa-trash-o font-medium-3 mr-2"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
