    <div class="main-panel">
        <div class="main-content">
          <div class="content-wrapper">
            <div class="container-fluid"><!-- Wizard Starts -->
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="content-header">Add Product</h2>
                    </div>
                </div>
                <section id="icon-tabs">
                <div class="row">
                    <div class="col-12">
                    <div class="card">
                        
                        <div class="card-body">
                        <div class="card-block">
                        <form id="form" action="<?= base_url(); ?>products/save" method="POST" class="icons-tab-steps wizard-circle form form-horizontal" enctype="multipart/form-data">

                            <!-- Step 1 -->
                            <h6>Step 1</h6>
                            <fieldset>
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for=" ">Product No</label>
                                    <input type="text" class="form-control" id=" " required name="product_no">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="lastName2">Product Name</label>
                                        <input type="text" class="form-control" id="lastName2" required name="name" placeholder="Product Name">
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="location2">Product Type</label>
                                    <select class="custom-select form-control" required id="location2" name="product_type">
                                        <option value="">Select product type</option>
                                        <option value="0">Current Stock</option>
                                        <option value="1">Future Fire</option>
                                    </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for=" ">Product Price</label>
                                    <input type="number" class="form-control" id=" " required name="price">
                                    </div>
                                </div>
                                
                                </div>
                                <div class="row">
                                    <div class="col-md-6 p-0">
                                        <div class="form-group">
                                            <label class="">Product Information: </label>
                                            <div class="col-md-12">
                                                <textarea rows="4" class="form-control" required name="product_info"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 p-0">
                                        <div class="form-group">
                                            <label class="" >Composition & Care: </label>
                                            <div class="col-md-12">
                                                <textarea rows="4" class="form-control" required name="product_composition"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 p-0">
                                        <div class="form-group">
                                            <label class="" >Shipping & Return: </label>
                                            <div class="col-md-12">
                                                <textarea rows="4" class="form-control" required name="product_shipping"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="shortDescription2">Product Description</label>
                                            <textarea name="shortDescription" id="shortDescription2" rows="2" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div> -->
                                <!-- <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="phoneNumber2">Product Price</label>
                                    <input type="tel" class="form-control" id="phoneNumber2" name="phoneNumber2">
                                    </div>
                                </div>
                                </div> -->
                            </fieldset>
                            <!-- Step 2 -->
                            <h6>Step 2</h6>
                            <fieldset>
                                <div class="row">
                                <!--Shopping cart starts-->
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                            <table id="recent-orders" class="table table-hover table-xl mb-0">
                                                <thead>
                                                    <tr>
                                                        <th class="border-top-0">.</th>                                
                                                        <th class="border-top-0">XS</th>
                                                        <th class="border-top-0">S</th>
                                                        <th class="border-top-0">M</th>
                                                        <th class="border-top-0">L</th>
                                                        <th class="border-top-0">XL</th>
                                                        <th class="border-top-0">XXL</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-truncate"><button class="btn btn-block" style="background-color: black; color:white;">Black</button></td>
                                                        <td class="text-truncate"><input type="text" class="form-control" id="b0" name="black[]"></td>
                                                        <td class="text-truncate"><input type="text" class="form-control" id="b1" name="black[]"></td>
                                                        <td class="text-truncate"><input type="text" class="form-control" id="b2" name="black[]"></td>
                                                        <td class="text-truncate"><input type="text" class="form-control" id="b3" name="black[]"></td>
                                                        <td class="text-truncate"><input type="text" class="form-control" id="b4" name="black[]"></td>
                                                        <td class="text-truncate"><input type="text" class="form-control" id="b5" name="black[]"></td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td class="text-truncate"><button class="btn btn-block" style="background-color: white; color:black; border: 1px solid black; " >white</button></td>
                                                        <td class="text-truncate"><input type="text" class="form-control" id="w0" name="white[]"></td>
                                                        <td class="text-truncate"><input type="text" class="form-control" id="w1" name="white[]"></td>
                                                        <td class="text-truncate"><input type="text" class="form-control" id="w2" name="white[]"></td>
                                                        <td class="text-truncate"><input type="text" class="form-control" id="w3" name="white[]"></td>
                                                        <td class="text-truncate"><input type="text" class="form-control" id="w4" name="white[]"></td>
                                                        <td class="text-truncate"><input type="text" class="form-control" id="w5" name="white[]"></td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td class="text-truncate"><button class="btn btn-block" style="background-color: red; color:white; " >Red</button></td>
                                                        <td class="text-truncate"><input type="text" class="form-control" id="r0" name="red[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="r1" name="red[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="r2" name="red[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="r3" name="red[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="r4" name="red[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="r5" name="red[]"></td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td class="text-truncate"><button class="btn btn-block" style="background-color: yellow; color:white; " >Yellow</button></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="y0" name="yellow[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="y1" name="yellow[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="y2" name="yellow[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="y3" name="yellow[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="y4" name="yellow[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="y5" name="yellow[]"></td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td class="text-truncate"><button class="btn btn-block" style="background-color: blue; color:white; " >Blue</button></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="b0" name="blue[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="b1" name="blue[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="b2" name="blue[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="b3" name="blue[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="b4" name="blue[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="b5" name="blue[]"></td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td class="text-truncate"><button class="btn btn-block" style="background-color: orange; color:white; " >Orange</button></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="o0" name="orange[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="o1" name="orange[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="o2" name="orange[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="o3" name="orange[]"></td>
                                                        <td class="text-truncate"><input type="text" class="form-control" id="o4" name="orange[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id="o5" name="orange[]"></td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td class="text-truncate"><button class="btn btn-block" style="background-color: purple; color:white; " >Purple</button></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id=" " name="purple[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id=" " name="purple[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id=" " name="purple[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id=" " name="purple[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id=" " name="purple[]"></td>
                                                        <td class="text-truncate"><input type="text"  class="form-control" id=" " name="purple[]"></td>
                                                        
                                                    </tr>
                                                    
                                                </tbody>
                                            </table>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for=" ">Total Product Quantity</label>
                                                    <input type="number" class="form-control" required id="total_qty" value="0" name="total_qty">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for=" ">Fit & Size</label>
                                                    <input type="text" class="form-control" required id=" " name="fit_and_size">
                                                </div>
                                            </div>

                                        </div>
                                        
                                </div>
                                
                                </div>
                            </fieldset>
                            <!-- Step 3 -->
                            <h6>Step 3</h6>
                            <fieldset>
                                <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <label>Upload Product Images</label>
                                    <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Image 1</span>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" name="images[]" required class="custom-file-input" id=" ">
                                        <label class="custom-file-label" for=" ">Choose file</label>
                                    </div>
                                    </div>
                                    <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Image 2</span>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" name="images[]" class="custom-file-input" id=" ">
                                        <label class="custom-file-label" for=" ">Choose file</label>
                                    </div>
                                    </div>
                                    <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Image 3</span>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" name="images[]" class="custom-file-input" id=" ">
                                        <label class="custom-file-label" for=" ">Choose file</label>
                                    </div>
                                    </div>
                                    <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Image 4</span>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" name="images[]" class="custom-file-input" id=" ">
                                        <label class="custom-file-label" for=" ">Choose file</label>
                                    </div>
                                    </div>
                                    <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Image 5</span>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" name="images[]" class="custom-file-input" id=" ">
                                        <label class="custom-file-label" for=" ">Choose file</label>
                                    </div>
                                    </div>
                                </div>
                                
                                </div>
                            </fieldset>
                            
                            </form>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </section>
                <!-- Wizard Ends -->
            </div>
          </div>
        </div>

    






