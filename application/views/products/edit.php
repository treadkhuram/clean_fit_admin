<div class="main-panel">
        <div class="main-content">
          <div class="content-wrapper">
            <div class="container-fluid"><!-- Wizard Starts -->
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="content-header">Add Product</h2>
                    </div>
                </div>
                <section id="icon-tabs">
                <div class="row">
                    <div class="col-12">
                    <div class="card">
                        
                        <div class="card-body">
                        <div class="card-block">
                        <form id="form" action="<?= base_url(); ?>products/update" method="POST" class="icons-tab-steps wizard-circle form form-horizontal" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?= $products->id?>">
                            <!-- Step 1 -->
                            <h6>Step 1</h6>
                            <fieldset>
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for=" ">Product No</label>
                                    <input type="text" class="form-control" id=" " value="<?= $products->product_no?>" name="product_no" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="lastName2">Product Name</label>
                                        <input type="text" class="form-control" id="lastName2" name="name" value="<?= $products->name?>" required placeholder="Product Name">
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="location2">Product Type</label>
                                    <select class="custom-select form-control" required id="location2" required name="product_type">
                                        <option value="" <?php if($products->product_type == ''){echo 'selected';} ?>>Select product type</option>
                                        <option value="0" <?php if($products->product_type == 0){echo 'selected';} ?>>Current Stock</option>
                                        <option value="1" <?php if($products->product_type == 1){echo 'selected';} ?>>Future Fire</option>
                                    </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for=" ">Product Price</label>
                                    <input type="number" class="form-control" id=" " value="<?= $products->price?>" required name="price">
                                    </div>
                                </div>
                                
                                </div>
                                <div class="row">
                                    <div class="col-md-6 p-0">
                                        <div class="form-group">
                                            <label class="">Product Information: </label>
                                            <div class="col-md-12">
                                                <textarea rows="4" class="form-control" required name="product_info"><?= $products->product_info?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 p-0">
                                        <div class="form-group">
                                            <label class="" >Composition & Care: </label>
                                            <div class="col-md-12">
                                                <textarea rows="4" class="form-control" required name="product_composition"><?= $products->product_composition?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 p-0">
                                        <div class="form-group">
                                            <label class="" >Shipping & Return: </label>
                                            <div class="col-md-12">
                                                <textarea rows="4" class="form-control" required name="product_shipping"><?= $products->product_shipping?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="shortDescription2">Product Description</label>
                                            <textarea name="shortDescription" id="shortDescription2" rows="2" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div> -->
                                <!-- <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="phoneNumber2">Product Price</label>
                                    <input type="tel" class="form-control" id="phoneNumber2" name="phoneNumber2">
                                    </div>
                                </div>
                                </div> -->
                            </fieldset>
                            <!-- Step 2 -->
                            <h6>Step 2</h6>
                            <fieldset>
                                <div class="row">
                                <!--Shopping cart starts-->
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                            <table id="recent-orders" class="table table-hover table-xl mb-0">
                                                <thead>
                                                    <tr>
                                                        <th class="border-top-0">.</th>                                
                                                        <th class="border-top-0">XS</th>
                                                        <th class="border-top-0">S</th>
                                                        <th class="border-top-0">M</th>
                                                        <th class="border-top-0">L</th>
                                                        <th class="border-top-0">XL</th>
                                                        <th class="border-top-0">XXL</th>
                                                    </tr>
                                                </thead>
                                                <?php $color = json_decode($products->colors); ?>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-truncate"><button class="btn btn-block" style="background-color: black; color:white;">Black</button></td>
                                                        <?php foreach($color->black as $c){?>
                                                            <td class="text-truncate"><input type="text" class="form-control" value="<?= $c?>" id="" name="black[]"></td>    
                                                        <?php } ?>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-truncate"><button class="btn btn-block" style="background-color: white; color:black; border: 1px solid black; " >white</button></td>
                                                        <?php foreach($color->white as $c){?>
                                                            <td class="text-truncate"><input type="text" class="form-control" value="<?= $c?>" id="" name="white[]"></td>
                                                        <?php } ?>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td class="text-truncate"><button class="btn btn-block" style="background-color: red; color:white; " >Red</button></td>
                                                        <?php foreach($color->red as $c){?>
                                                            <td class="text-truncate"><input type="text" class="form-control" value="<?= $c?>" id="" name="red[]"></td>
                                                        <?php } ?>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td class="text-truncate"><button class="btn btn-block" style="background-color: yellow; color:white; " >Yellow</button></td>
                                                        <?php foreach($color->yellow as $c){?>
                                                            <td class="text-truncate"><input type="text" class="form-control" value="<?= $c?>" id="" name="yellow[]"></td>
                                                        <?php } ?>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-truncate"><button class="btn btn-block" style="background-color: blue; color:white; " >Blue</button></td>
                                                        <?php foreach($color->blue as $c){?>
                                                            <td class="text-truncate"><input type="text" class="form-control" value="<?= $c?>" id="" name="blue[]"></td>
                                                        <?php } ?>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-truncate"><button class="btn btn-block" style="background-color: orange; color:white; " >Orange</button></td>
                                                        <?php foreach($color->orange as $c){?>
                                                            <td class="text-truncate"><input type="text" class="form-control" value="<?= $c?>" id="" name="orange[]"></td>
                                                        <?php } ?>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-truncate"><button class="btn btn-block" style="background-color: purple; color:white; " >Purple</button></td>
                                                        <?php foreach($color->purple as $c){?>
                                                            <td class="text-truncate"><input type="text" class="form-control" value="<?= $c?>" id="" name="purple[]"></td>
                                                        <?php } ?>
                                                    </tr>
                                                    
                                                </tbody>
                                            </table>
                                        </div><br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for=" ">Total Product Quantity</label>
                                                    <input type="number" class="form-control" required id="total_qty" value="<?= $products->total_qty?>" name="total_qty">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for=" ">Fit & Size</label>
                                                    <input type="text" class="form-control" required id=" " value="<?= $products->fit_and_size?>" name="fit_and_size">
                                                </div>
                                            </div>

                                        </div>
                                        
                                </div>
                                
                                </div>
                            </fieldset>
                            <!-- Step 3 -->
                            <h6>Step 3</h6>
                            <fieldset>
                                <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <label>Upload Product Images</label>
                                    <?php $images = json_decode($products->images);
                                            $counter = 0;
                                        foreach($images as $k=>$img){
                                            $counter++;
                                    ?>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Image <?= $counter?></span>
                                            <span class="input-group-text"><img src="<?= base_url()?>assets/uploaded_images/<?= $img?>" width="20px" alt=""></span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" name="images[]" class="custom-file-input" id=" ">
                                            <label class="custom-file-label" for=" ">Choose file</label>
                                        </div>
                                    </div>
                                    <?php }?>
                                    <?php if($counter < 5){
                                        for($i=$counter; $i<6; $i++){
                                         ?>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Image <?= $i?></span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" name="images[]" class="custom-file-input" id=" ">
                                            <label class="custom-file-label" for=" ">Choose file</label>
                                        </div>
                                    </div>
                                    <?php }}?>
                                </div>
                                
                                </div>
                            </fieldset>
                            
                            </form>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </section>
                <!-- Wizard Ends -->
            </div>
          </div>
        </div>

    










<!-- 
<div class="main-panel">
    <div class="main-content">
        <div class="content-wrapper">
            <div class="container-fluid">
                <section id="horizontal-form-layouts">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title-wrap bar-warning">
                                        <h4 class="card-title" id="horz-layout-colored-controls">Edit Products</h4>
                                    </div>
                                    <p class="mb-0">You can add new products here by providing the below listed
                                        details.</p>
                                </div>
                                <div class="card-body">
                                    <div class="px-3">
                                        <form action="<?= base_url(); ?>products/edit/<?= $products_data->id?>" method="POST"
                                              class="form form-horizontal" enctype="multipart/form-data">
                                            <div class="form-body">
                                                <h4 class="form-section">
                                                    <i class="icon-direction"></i> Product detail</h4>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Product Name: </label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control border-primary"
                                                                       name="product_name" value="<?= $products_data->name ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Price: </label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control border-primary"
                                                                       name="price" value="<?= $products_data->price ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Quantity: </label>
                                                            <div class="col-md-9">
                                                                <input type="number" class="form-control border-primary"
                                                                       name="quantity" value="<?= $products_data->quantity ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">SKU: </label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control border-primary"
                                                                       name="sku" value="<?= $products_data->sku ?>">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Colors: </label>
                                                            <div class="col-md-9">
                                                                <select name="color[]" class="form-control border-primary select2" multiple="multiple">
                                                                    
                                                                    <?php foreach ($colors as $col){ ?>
                                                                        <option value="<?= $col->id ?>" <?php foreach($color_data as $c){
                                                                            if($c->id == $col->id){ echo 'selected'; break;}
                                                                        } ?>><?= $col->name ?></option>
                                                                    <?php } ?>
                                                                    
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Size: </label>
                                                            <div class="col-md-9">
                                                                <select name="size[]" class="form-control border-primary select2" multiple="multiple">
                                                                    <?php foreach ($sizes as $s){ ?>
                                                                        <option value="<?= $s->id ?>" <?php foreach($size_data as $sd){
                                                                            if($sd->id == $s->id){echo 'selected'; break;}
                                                                        } ?>><?= $s->name ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Store: </label>
                                                            <div class="col-md-9">
                                                                <select name="store" class="form-control border-primary select2">
                                                                    <option selected disabled value="">Select Store</option>
                                                                    <?php foreach ($stores as $s){ ?>
                                                                        <option value="<?= $s->id ?>" <?php if($store_data->id == $s->id){echo 'selected';} ?> ><?= $s->name ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h4 class="form-section">
                                                    <i class="icon-camera" id="im-sec"></i>Product images</h4>
                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <label class="col-md-3 label-control">Select multiple images: </label>
                                                            <div class="col-md-8">
                                                                <div class="row">

                                                                    <?php  if(($products_data->images) != 'null'){?>
                                                                          <?php  $images = json_decode($products_data->images); foreach($images as $k=>$img){?>
                                                                            <div class="col-md-2 text-center" id="<?= $k?>">
                                                                                <a href="#" class="btn btn-danger btn-sm" onclick="removeImg('<?= $img?>', '<?= $products_data->id ?>', '<?= $k?>')">x</a>    
                                                                                <img id="" src="<?= base_url()?>assets/uploaded_images/<?= $img?>"  height="60px" width="50px"  alt="">
                                                                            </div>
                                                                       <?php } }else{echo 'empty';}?>
                                                                    
                                                                </div><br>
                                                                <input type="file" name="images[]" id="images" multiple >
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <h4 class="form-section">
                                                    <i class="icon-globe"></i>Product more detail</h4>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" >Product Information: </label>
                                                            <div class="col-md-9">
                                                                <textarea rows="4" class="widgEditor form-control border-primary" name="information"><?= $products_data->product_info?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" >Composition & Care: </label>
                                                            <div class="col-md-9">
                                                                <textarea rows="4" class="widgEditor form-control border-primary" name="composition"><?= $products_data->product_composition?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" >Shipping & Return: </label>
                                                            <div class="col-md-9">
                                                                <textarea rows="4" class="widgEditor form-control border-primary" name="shipping"><?= $products_data->product_shipping?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-actions right">
                                                <button type="submit" name="submit" class="btn btn-success">Submit</button>
                                            </div>
                                    </div>

                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
            </div>

            </section>
        </div>
    </div>
</div> -->




<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
function removeImg(img, id, k){

    // var img = $('#img').text();
    // console.log(img);
    $.ajax({
        url: '<?= base_url(); ?>products/removeImg',
        method: 'POST',
        data: {img: img, id: id},
        success: function(res){
            console.log(res);
            $('#'+k).remove();
        }
    });
}


</script>

