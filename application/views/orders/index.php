<div class="main-panel">
    <div class="main-content">
        <div class="content-wrapper">
            <div class="container-fluid"><!--Extended Table starts-->
                <div class="row">
                    <div class="col-8">
                        <h2 class="content-header">Users Orders</h2>
                        <p class="content-sub-header">Users orders.</p>
                    </div>
                    <!--                    <div class="col-4">-->
                    <!--                        <h2 class="content-header pull-right">-->
                    <!--                            <a href="--><?//= base_url('products/add')?><!--" class="btn btn-social btn-round  btn-outline-facebook">-->
                    <!--                                <span class="ft-plus"></span> Add Product</a>-->
                    <!--                        </h2>-->
                    <!--                    </div>-->
                </div>

                <section id="shopping-cart">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-block">
                                        <div class="table-responsive">
                                            <table id="recent-orders" class="table table-hover table-xl mb-0">
                                                <thead>
                                                <tr>
                                                    <th class="border-top-0">#</th>
                                                    <th class="border-top-0">Name</th>
                                                    <th class="border-top-0">Order</th>
                                                    <th class="border-top-0">Actions</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
