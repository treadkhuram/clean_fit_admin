<div class="main-panel">
    <div class="main-content">
        <div class="content-wrapper">
            <div class="container-fluid">
                <section id="horizontal-form-layouts">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title-wrap bar-warning">
                                        <h4 class="card-title" id="horz-layout-colored-controls">Store Registration</h4>
                                    </div>
                                    <p class="mb-0">You can register the store here by providing the below listed
                                        details.</p>
                                </div>
                                <div class="card-body">
                                    <div class="px-3">
                                        <form action="<?= base_url(); ?>stores/edit/<?= $store_data->id ?>" method="POST"
                                              class="form form-horizontal" enctype="multipart/form-data">
                                            <div class="form-body">
                                                <h4 class="form-section">
                                                    <i class="icon-direction"></i> Store Detail</h4>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Store Name: </label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control border-primary"
                                                                       name="store_name" value="<?= $store_data->name ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Phone No: </label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control border-primary"
                                                                       name="phone" value="<?= $store_data->phone ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Email: </label>
                                                            <div class="col-md-9">
                                                                <input type="email" class="form-control border-primary"
                                                                       name="email" value="<?= $store_data->email ?>" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Website: </label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control border-primary"
                                                                       name="website" value="<?= $store_data->website ?>">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Password: </label>
                                                            <div class="col-md-9">
                                                                <input type="password" value="<?= $store_data->password?>" class="form-control border-primary"
                                                                       name="password">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Status: </label>
                                                            <div class="col-md-9">
                                                                <select name="status" required id="" class="form-control">
                                                                    <option value="" <?php if($store_data->status == ''){echo 'selected';} ?>>Select Status...</option>
                                                                    <option value="0" <?php if($store_data->status == 0){echo 'selected';} ?>>Active</option>
                                                                    <option value="1" <?php if($store_data->status == 1){echo 'selected';} ?>>Inactive</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Store Logo: </label>
                                                            <label class="col-md-3 label-control"><?= $store_data->store_logo ?> </label>
                                                            <div class="col-md-9">
                                                                <input type="file" name="logo" id="logo" >
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <h4 class="form-section">
                                                    <i class="icon-globe"></i> Store Address</h4>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" >City: </label>
                                                            <div class="col-md-9">
                                                                <input type="text" value="<?= $store_data->city ?>" class="form-control border-primary" name="city">
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 label-control">Country: </label>
                                                                <div class="col-md-9">
                                                                    <input type="text" value="<?= $store_data->country ?>" class="form-control border-primary"name="country">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">

                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" >Company Address: </label>
                                                            <div class="col-md-9">
                                                                <textarea rows="4"  class="form-control border-primary" name="company_address"><?= $store_data->address ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="form-actions right">
                                                <button type="submit" name="submit" class="btn btn-success">Submit</button>
                                            </div>
                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        </div>
    </div>
