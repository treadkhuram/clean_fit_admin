<div class="main-panel">
    <div class="main-content">
        <div class="content-wrapper">
            <div class="container-fluid">
                <section id="horizontal-form-layouts">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title-wrap bar-warning">
                                        <h4 class="card-title" id="horz-layout-colored-controls">Store Registration</h4>
                                    </div>
                                    <p class="mb-0">You can register the store here by providing the below listed
                                        details.</p>
                                </div>
                                <div class="card-body">
                                    <div class="px-3">
                                        <form action="<?= base_url(); ?>stores/add" method="POST"
                                              class="form form-horizontal" enctype="multipart/form-data">
                                            <div class="form-body">
                                                <h4 class="form-section">
                                                    <i class="icon-direction"></i> Store Detail</h4>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Store Name: </label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control border-primary"
                                                                       name="store_name" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Phone No: </label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control border-primary"
                                                                       name="phone" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Email: </label>
                                                            <div class="col-md-9">
                                                                <input type="email" class="form-control border-primary"
                                                                       name="email" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Website: </label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control border-primary"
                                                                       name="website">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Password: </label>
                                                            <div class="col-md-9">
                                                                <input type="password" class="form-control border-primary"
                                                                       name="password">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Status: </label>
                                                            <div class="col-md-9">
                                                                <select name="status" required id="" class="form-control">
                                                                    <option value="">Select Status...</option>
                                                                    <option value="0">Active</option>
                                                                    <option value="1">Inactive</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control">Store Logo: </label>
                                                            <div class="col-md-9">
                                                                <input type="file" name="logo" id="logo" required>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <h4 class="form-section">
                                                    <i class="icon-globe"></i> Store Address</h4>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" >City: </label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control border-primary" name="city">
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group row">
                                                                <label class="col-md-3 label-control">Country: </label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control border-primary"name="country">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">

                                                        <div class="form-group row">
                                                            <label class="col-md-3 label-control" >Company Address: </label>
                                                            <div class="col-md-9">
                                                                <textarea rows="4" class="form-control border-primary" name="company_address"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="form-actions right">
                                                <button type="submit" name="submit" class="btn btn-success">Submit</button>
                                            </div>
                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        </div>
    </div>
