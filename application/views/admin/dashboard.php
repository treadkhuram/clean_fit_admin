

<?php if($this->session->userdata('admin_logged_in')){ ?>
<div class="main-panel">
        <div class="main-content">
          <div class="content-wrapper">
            <div class="container-fluid"><!--Statistics cards Starts-->

              <section id="minimal-statistics">
                
                <div class="row">
                  <div class="col-xl-4 col-lg-6 col-12">
                    <div class="card">
                      <div class="card-body">
                        <div class="px-3 py-3">
                          <div class="media">
                            <div class="media-body text-left align-self-center">
                              <i class="icon-basket-loaded info font-large-2 float-left"></i>
                            </div>
                            <div class="media-body text-right">
                              <h3><?= $total_stores?></h3>
                              <span>Total Stores</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-4 col-lg-6 col-12">
                    <div class="card">
                      <div class="card-body">
                        <div class="px-3 py-3">
                          <div class="media">
                            <div class="media-body text-left align-self-center">
                              <i class="icon-users font-large-2 info float-left"></i>
                            </div>
                            <div class="media-body text-right">
                              <h3><?= $total_users?></h3>
                              <span>Total App Users</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-4 col-lg-6 col-12">
                    <div class="card">
                      <div class="card-body">
                        <div class="px-3 py-3">
                          <div class="media">
                            <div class="media-body text-left align-self-center">
                              <i class="icon-wallet info font-large-2 float-left"></i>
                            </div>
                            <div class="media-body text-right">
                              <h3>$0</h3>
                              <span>Earned Commission</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                </div>
                
              </section>

              <!--Line with Area Chart 1 Starts-->
              <div class="row match-height">
              	<div class="col-xl-12 col-lg-12 col-12">
              		<div class="card">
              			<div class="card-header">
                      		<div class="card-title-wrap bar-success">
              					<h4 class="card-title">Mobile App User Growth</h4>
              				</div>
              			</div>
              			<div class="card-body">
              				<div class="card-block">
              					<div id="line-chart" class="height-300 lineChart lineChartShadow">						
              					</div>
              				</div>
              			</div>
              		</div>
              	</div>
              </div>
              <!--Line with Area Chart 1 Ends-->


            </div>
          </div>
        </div>

<?php }?>
<?php if($this->session->userdata('store_logged_in')){ ?>


  <div class="main-panel">
        <div class="main-content">
          <div class="content-wrapper">
            <div class="container-fluid"><!--Statistics cards Starts-->
<div class="row">
    <div class="col-xl-6 col-lg-6 col-md-6 col-12">
        <div class="card bg-white">
            <div class="card-body">
                <div class="card-block pt-2 pb-0">
                    <div class="media">
                        <div class="media-body text-left">
                            <h4 class="font-medium-5 card-title mb-0"><?= $total_products?></h4>
                            <span>Total Products</span>
                        </div>
                        <div class="media-right text-right">
                            <i class="icon-handbag font-large-1 red"></i>
                        </div>
                    </div>
                </div>
                <div id="widget-line-area" class="height-150 WidgetAreaChart WidgetAreaChart1 WidgetAreaChartshadow mb-2">
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6 col-lg-6 col-md-6 col-12">
        <div class="card bg-white">
            <div class="card-body">
                <div class="card-block pt-2 pb-0">
                    <div class="media">
                        <div class="media-body text-left">
                            <h4 class="font-medium-5 card-title mb-0">0</h4>
                            <span>Total Orders</span>
                        </div>
                        <div class="media-right text-right">
                            <i class="icon-basket-loaded font-large-1 purple"></i>
                        </div>
                    </div>
                </div>
                <div id="widget-line-area2" class="height-150 WidgetAreaChart WidgetAreaChart2 WidgetAreaChartshadow mb-2">
                </div>

            </div>
        </div>
    </div>

</div>
<!--Statistics cards Ends-->

            </div>
          </div>
        </div>

       


<?php }?>