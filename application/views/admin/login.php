<!DOCTYPE html>
<html lang="en" class="loading">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

    <title>Stores</title>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">

    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900%7CMontserrat:300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>fonts/simple-line-icons/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>vendors/css/perfect-scrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>vendors/css/prism.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>vendors/css/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/'); ?>css/app.css">
</head>

<body data-col="1-column" class=" 1-column  blank-page blank-page">
<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="wrapper"><!--Login Page Starts-->
    <section id="login">
        <div class="container-fluid">
            <div class="row full-height-vh">
                <div class="col-12 d-flex align-items-center justify-content-center" style="background-color: #ffffff !important;">
                    <div class="card px-4 py-2 box-shadow-2 width-400">
                        <?php if($this->session->flashdata('attenticaion')):?>
                            <?php echo $this->session->flashdata('attenticaion');?>
                        <?php endif;?>
                        <div class="card-header text-center">
                            <img src="<?php echo base_url('assets/img/logo-dark.png'); ?>" alt="company-logo" class="mb-3"  width="80">
                             <h4 class="text-uppercase text-bold-400 grey darken-1">LOGIN</h4>
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                <form action="<?php echo base_url(); ?>Login/authentication" method="POST" >
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="test" class="form-control form-control-lg" name="username" id="inputEmail" placeholder="Username" required email>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="password" class="form-control form-control-lg" name="password" id="inputPass" placeholder="Password" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0 ml-5">
                                                    <input type="checkbox" class="custom-control-input" checked id="rememberme">
                                                    <label class="custom-control-label float-left" for="rememberme">Remember Me</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="text-center col-md-12">
                                            <button type="submit" class="btn btn-danger btn-block px-4 py-2 text-uppercase white font-small-4 box-shadow-2 border-0">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Login Page Ends-->
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->
<script src="<?php echo base_url('assets/'); ?>vendors/js/core/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/js/core/popper.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/js/core/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/js/perfect-scrollbar.jquery.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/js/prism.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/js/jquery.matchHeight-min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/js/screenfull.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>vendors/js/pace/pace.min.js"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="<?php echo base_url('assets/'); ?>vendors/js/chartist.min.js"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN CONVEX JS-->
<script src="<?php echo base_url('assets/'); ?>js/app-sidebar.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/notification-sidebar.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/customizer.js"></script>
<!-- END CONVEX JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url('assets/'); ?>js/dashboard-ecommerce.js"></script>
</body>

</html>